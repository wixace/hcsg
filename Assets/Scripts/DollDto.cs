﻿using System;

[Serializable]
public class DollDto
{
	public int    Id;
	public string Name;
	public int    Level;
	public long   Price;
	public string Appreciation;
	public int    Health;
	public int    Damage;
	public int    Power;
	public long   Turnout;
	public int    SceneId;
	public string Message;
	public string Spr;
}