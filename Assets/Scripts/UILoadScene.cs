﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Fs.Libs.UI {

    [RequireComponent(typeof(Button))]
    public class UILoadScene : MonoBehaviour {
        [SerializeField]
        [Tooltip("Parameter take string or integer values.")]
        private string _scene = "Main";

        [SerializeField]
        private bool _additive;

        private bool _isLocked;

        protected void Start() {
            GetComponent<Button>().onClick.AddListener(() => {
                if(_isLocked)
                    return;
                _isLocked=true;
                if(_additive)
                    LoadAdditive();
                else
                    LoadScene(_scene);
            });
        }

        /// <summary>
        /// 加载场景
        /// </summary>
        /// <param name="scene"></param>
        public void LoadScene(string scene) {
            int result;
            if(int.TryParse(_scene,out result)) {
                SceneManager.LoadScene(result);
            } else {
                SceneManager.LoadScene(_scene);
            }
        }

        /// <summary>
        /// 叠加加载场景
        /// </summary>
        private void LoadAdditive() {
            SceneManager.LoadScene(_scene,LoadSceneMode.Additive);
        }
    }
}
