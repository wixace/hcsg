﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UISlideShow : MonoBehaviour
{
    [SerializeField] private GameObject[] _list; //顺序显示的物体

    [SerializeField] private Button _nextBtn;

    [SerializeField] private UnityEvent _onPageEnd;

    private int _index; //当前显示顺序


    void Start() =>  _nextBtn.onClick.AddListener(Next);

    // Update is called once per frame
    void Next() {
        _list[_index++].gameObject.SetActive(false);
        if (_index < _list.Length)
            _list[_index].gameObject.SetActive(true);
        else
            _onPageEnd.Invoke();
    }
}
