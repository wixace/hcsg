﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;

public class RemoteLoader : MonoBehaviour {
	

	/// <summary>
	/// without buildtarget
	/// </summary>
	//[SerializeField] private string remotePath = "http://localhost:8000/HotfixA";

	
	[SerializeField] private string catalogUrl = "http://localhost:8000/HotfixA/StandaloneOSX/catalog_2020.06.08.09.06.48.json";
	
	[SerializeField] private string key = "TestCanvas";
	//public static string Url;
	
	void Start() {
	//	Url = remotePath;
		AsyncOperationHandle<IResourceLocator> loadContentCatalogAsync = Addressables.LoadContentCatalogAsync(catalogUrl);
		loadContentCatalogAsync.Completed += OnCompleted;    
	}
 
	private void OnCompleted(AsyncOperationHandle<IResourceLocator> obj)
	{
		IResourceLocator resourceLocator = obj.Result;
		foreach (var v in resourceLocator.Keys) {
			print(v);
		}
		resourceLocator.Locate(key, typeof(GameObject), out IList<IResourceLocation> locations);
		IResourceLocation resourceLocation     = locations[0];
		GameObject        resourceLocationData =(GameObject) resourceLocation.Data;
		Addressables.InstantiateAsync(resourceLocation);  
		//Addressables.LoadSceneAsync("Game"); 
	}

}