﻿using System;

#pragma warning disable
namespace Assets.Shaddock.Scripts.Foundation.Base
{
	public abstract class Singleton<T>  where T : class,new()

	{

		protected static T _instance = default(T);

		public static T Instance
		{
			get
			{
				if(_instance == null)
				{
					//GameObject go = new GameObject(typeof(T).Name);
					//_instance = go.AddComponent<T>();
					_instance = new T();
                    
				}
				return _instance;
			}
			set
			{
				_instance = value;
			}
		}
		//protected Singleton()
		//{
		//    if(_instance != null)
		//        throw new SingletonException(typeof(T).ToString() + " Singleton Instance is not null!");
		//}
		public static void Create()
		{
			_instance = (T)Activator.CreateInstance(typeof(T), true);

			return;
		}
		public static void Destroy()
		{

			_instance = null;

			return;
		}
		public class SingletonException:Exception
		{
			public SingletonException(string msg):base(msg)
			{
    
			}
		}
	}
}
 

