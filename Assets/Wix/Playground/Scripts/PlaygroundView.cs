﻿using System.Reflection;
using Assets.Scripts.Utils;
using uMVVM.Sources.Infrastructure;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Playground {
	public class PlaygroundView :ViewBase<PlaygroundViewModel> {

		public PlaygroundViewModel ViewModel => BindingContext;

		public Text MoneyText, TurnoutText;

		private TurnoutConfig _turnoutConfig;

		void Awake() {
			_turnoutConfig=Resources.Load<TurnoutConfig>("TurnoutConfig");
		}

		void Start() {
			InvokeRepeating("UpdateMoney",0,1);
		}


		protected override void OnInitialize() {
			base.OnInitialize();
			Binder.Add<decimal>("Money",OnMoneyPropertyValueChanged);
			Binder.Add<decimal>("Turnout",OnTurnoutPropertyValueChanged);
		}

		private void OnMoneyPropertyValueChanged(decimal oldValue,decimal newValue) => MoneyText.text=GetUnitText(newValue);

		private void OnTurnoutPropertyValueChanged(decimal oldValue,decimal newValue) => TurnoutText.text=GetUnitText(newValue)+"(秒)";

		void UpdateMoney() => ViewModel.UpdateInfo();

		private string GetUnitText(decimal value) {
			//var len = value.ToString().Length;
			var offset = _turnoutConfig.Offset;

			var result = DecimalMath.Log10(value*1000)/3-1;
			//print("normal log:" + Mathf.Log10((float) (result*1000)));
			//print("log:"+DecimalMath.Log10(value*1000)+"result" + result+" value:"+value+" offset:"+offset);
			//return value.ToString().Substring(0,Mathf.Clamp(value.ToString(CultureInfo.InvariantCulture).Length,0,3))+_turnoutConfig.Units[(int)result].ToString();
			//string lastDigit = result/*>=1000?value.ToString().Split('.')[1]:string.Empty;*/
			return value.ToString().Substring(0,value.ToString().Length-3*(int)result)+_turnoutConfig.Units[(int)result];

		}
	}
}
