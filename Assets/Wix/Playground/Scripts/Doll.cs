﻿using UnityEngine;

public class Doll : MonoBehaviour {

	public int Id { get; set; }

	private SpriteRenderer _spriteRenderer;

	//private Transform _transform;
	public SpriteRenderer SpriteRenderer
	{
		get { return _spriteRenderer; }
		set { _spriteRenderer=value; }
	}

	// Use this for initialization
	void Awake() {
		_spriteRenderer=GetComponent<SpriteRenderer>();
		//_sprite. = Resources.Load<Sprite>("Playground/Character/b");
		//_transform=GetComponent<Transform>();
	}

	void LateUpdate() {
		transform.position=new Vector3(transform.position.x,transform.position.y,transform.position.y);
	}
	//void OnCollisionEnter2D(Collision2D coll) {
	//	if(coll.gameObject.CompareTag("Doll")) {
	//		Debug.Log("Bingo");
	//	}
	//}
}
