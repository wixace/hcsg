﻿using UnityEngine;

public class PlaygroundController : MonoBehaviour {

	public int CurrentSelected = 0;
	
	public void Purchase() {
		PlaygroundDispatcher.Instance.Purchase(0+CurrentSelected*5);
	}

	public void Purchase(int id) {
		PlaygroundManager.Instance.AddDoll(id,0);

	}

	public void Combine(int id, int scene) {
		PlaygroundManager.Instance.AddDoll(id,scene,true);
	}
	public void SetCurrentScene(int id) {
		CurrentSelected=id;
	}
}
