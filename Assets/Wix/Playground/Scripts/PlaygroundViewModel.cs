﻿using Assets.Scripts.Utils;
using uMVVM.Sources.Infrastructure;

namespace Assets.Scripts.Playground {
	public class PlaygroundViewModel : ViewModelBase {

		public readonly BindableProperty<decimal> Money = new BindableProperty<decimal>();

		public readonly BindableProperty<decimal> Turnout = new BindableProperty<decimal>();

		public readonly BindableProperty<int[]> Dolls = new BindableProperty<int[]>();

		public PlaygroundViewModel(PlaygroundModel playground) {
			Money.Value=playground.Money;
			Turnout.Value = playground.Turnout;
			Dolls.Value = playground.Dolls;
			PlaygroundManager.PlaygroundViewModel=this;
		}

		public void UpdateInfo() {
			Turnout.Value=PlaygroundUtil.GetTurnoutByDolls(Dolls.Value);
			Money.Value += Turnout.Value;
		}
	}
}
