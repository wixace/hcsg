﻿using System.Linq;
using Assets.Scripts.Caching;
using UnityEngine;

namespace Assets.Scripts.Utils {
	public static class PlaygroundUtil {

		private const string Path = "Playground/Character/";

		private const string Default = "a";
		public static Sprite GetSpriteById(int id)
		{
			var spr = DtoCache.ConfigDto.PlaygroundConfigDto.DollConfigs.FirstOrDefault(d => d.Id == id)?.Spr??Default;
			//Debug.Log(Path+spr);
			return Resources.Load<Sprite>(Path+spr);
		}

		public static int GetSceneById(int id)=> DtoCache.ConfigDto.PlaygroundConfigDto.DollConfigs.FirstOrDefault(d => d.Id==id)?.SceneId??0;

		public static decimal GetTurnoutByDolls(int[] dolls)
		{
			decimal totalTurnout=0;
			for (var i = 0; i < dolls.Length; i++)
			{
				int d = dolls[i];
				decimal turnout=DtoCache.ConfigDto.PlaygroundConfigDto.DollConfigs[i].Turnout * d;
				totalTurnout += turnout;
			}
			return totalTurnout;

		}


	}
}
