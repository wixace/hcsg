﻿using UnityEngine;

[CreateAssetMenu]
public class TurnoutConfig : ScriptableObject {

	public string[] Units = { "贝壳","铜币","银币","金币","银锭","金锭","白银","黄金" };

	public int Offset = 1000;

}
