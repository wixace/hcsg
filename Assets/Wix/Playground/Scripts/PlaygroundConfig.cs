﻿using UnityEngine;

[CreateAssetMenu()]
public class PlaygroundConfig : ScriptableObject {

	[System.Serializable]
	public struct Doll {
		public int Id;

		//public string Name;

		public Sprite Sprite;
	}

	public Doll[] Dolls;
}

