﻿using Assets.Scripts.Utils;
using Assets.Shaddock.Scripts.Effects.Object;
using UnityEngine;

public class DollDrag : DragObject {

	private const string Tag = "Doll";

	private Animator _animator;

	private Doll _doll;

	private Vector2 _targetPosition;

	public float MoveInterval = 3f;

	public float MoveSpeed = 1f;

	public float MoveDistance = 2f;

	private void Awake() {
		_animator=GetComponent<Animator>();
		_doll=GetComponent<Doll>();
		SetTarget();
	}

	void OnDisable() => CancelInvoke();

	void OnEnable() => InvokeRepeating("SetTarget",3,MoveInterval);


	void SetTarget() {
		//_targetPosition=new Vector2(Mathf.Clamp(transform.localPosition.x,Boundary.MinX,Boundary.MinX),Mathf.Clamp(transform.localPosition.y,Boundary.MinY,Boundary.MaxY))+Random.insideUnitCircle*MoveDistance;
		//_targetPosition=new Vector2(Mathf.Clamp(transform.localPosition.x,Boundary.MinX,Boundary.MinX),Mathf.Clamp(transform.localPosition.y,Boundary.MinY,Boundary.MaxY))+Random.insideUnitCircle*MoveDistance;
		var randomX = Random.Range(Boundary.MinX, Boundary.MaxX);
		var randomY = Random.Range(Boundary.MinY, Boundary.MaxY);
		_targetPosition=new Vector2(randomX,randomY);

		//print(randomX+","+randomY);

	}

	void OnDrawGizmos() {
		Gizmos.color=Color.red;
		//Gizmos.matrix=transform.localToWorldMatrix;
		Gizmos.DrawLine(transform.localPosition,_targetPosition);

		var topLeft = new Vector3(Boundary.MinX,Boundary.MaxY,transform.position.z);
		var topRight = new Vector3(Boundary.MaxX,Boundary.MaxY,transform.position.z);
		var btmRight = new Vector3(Boundary.MaxX,Boundary.MinY,transform.position.z);
		var btmLeft = new Vector3(Boundary.MinX,Boundary.MinY,transform.position.z);

		Gizmos.DrawLine(topLeft,topRight);
		Gizmos.DrawLine(topRight,btmRight);
		Gizmos.DrawLine(btmRight,btmLeft);
		Gizmos.DrawLine(btmLeft,topLeft);
	}
	void Update() {
		transform.localPosition=Vector2.MoveTowards(transform.position,_targetPosition,Time.deltaTime*MoveSpeed);
	}

	protected override void OnMouseUp() {
		base.OnMouseUp();
		//RaycastHit2D[] hit = Physics2D.BoxCastAll(Camera.main.ScreenToWorldPoint(Input.mousePosition),Vector2.one,0,-Vector2.up);
		RaycastHit2D[] hit = Physics2D.RaycastAll(transform.localPosition,Vector2.up,0);
		//RaycastHit2D[] hit = Physics2D.BoxCastAll(transform.position,Vector2.one,0, Vector2.zero);
		for(int i = 0; i<hit.Length; i++) {
			//if(hit[i].CompareTag(Tag)) {
			//	TrashMan.despawn(hit[i].gameObject);
			//	TrashMan.despawn(gameObject);
			//	print("hit");
			//	PlaygroundDispatcher.Instance.Combine(_doll.Id);
			//	break;
			//}

			var coll = hit[i].collider;
			if(coll.gameObject==gameObject)
				continue;

			if(coll.CompareTag(Tag)) {
				if(coll.GetComponent<Doll>().Id==_doll.Id) {
					TrashMan.despawn(coll.gameObject);
					TrashMan.despawn(gameObject);
					print("hit");
					PlaygroundManager.Instance.SetLastPos(transform.position);
					PlaygroundDispatcher.Instance.Combine(_doll.Id);
					PlaygroundManager.Instance.AddDoll(_doll.Id+1,PlaygroundUtil.GetSceneById(_doll.Id+1),true);
					break;
				}
			}
		}

	}
}
