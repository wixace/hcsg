﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Caching;
using Assets.Scripts.Playground;
using Assets.Scripts.Utils;
using Assets.Shaddock.Scripts.Foundation.Base;
using UnityEngine;
using UnityEngine.UI;
using ZBJ.PG;
using Boundary = Assets.Shaddock.Scripts.Effects.Object.Boundary;
using Random = UnityEngine.Random;

public class PlaygroundManager : USingleton<PlaygroundManager> {
	public Boundary Boundary;

	public GameObject Doll;

	public Transform[] Parents;

	public GameObject Smoke;

	public int Limit = 16;

	private Vector2 _lastPos;

	private int _count = 0;

	private int[] _sceneCapacity = new int[5];

	public static PlaygroundViewModel PlaygroundViewModel { get; set; }

	public PlaygroundView PlaygroundView;

	public GameObject Notice;

	public Text NoticeText;
	public void Start() {
		GameController.Init();
		Init();
		PlaygroundView.BindingContext = new PlaygroundViewModel(ModelManager.PlaygroundModel);
		PlaygroundViewModel.Money.Value = 20;
	}

	void Init() {
	//	Debug.Log("初始化");
		DtoCache.ConfigDto=new ConfigDto();
		DtoCache.ConfigDto.PlaygroundConfigDto=new PlaygroundConfigDto();
		DtoCache.ConfigDto.PlaygroundConfigDto.DollConfigs = GetCsv();

	}

	private DollDto[] GetCsv() {
	//	Debug.Log("初始化1");
		var        filesRecords = new List<DollDto>();
	//	Debug.Log("初始化2");
		string[][] csv          = CsvParser2.Parse(Resources.Load<TextAsset>("DollConfig").text);
		//Debug.Log("初始化3");
		print(Resources.Load<TextAsset>("DollConfig").text);
		var        len          = csv.Length;
		for (int i = 1; i < len; i++) {
			filesRecords.Add(new DollDto() {
				Id      = Convert.ToInt32(csv[i][0]), Name    = csv[i][1], Level = Convert.ToInt32(csv[i][2]),
				Price   = Convert.ToInt64(csv[i][3]), Turnout = Convert.ToInt64(csv[i][8]),
				SceneId = Convert.ToInt32(csv[i][10]),Spr = csv[i][12],
			});
		}

		return filesRecords.ToArray();
	}

	public void AddDoll(int id, int scene, bool compose = false) {
		if (compose) {
			_count--;
			_sceneCapacity[scene]--;
			PlaygroundViewModel.Dolls.Value[id - 1]--;
			PlaygroundViewModel.Dolls.Value[id]++;
		}
		else {
			Debug.Log( DtoCache.ConfigDto.PlaygroundConfigDto.DollConfigs[0].Price);
			if (_sceneCapacity[scene] + 1 >= Limit) {
				Debug.Log("容量已达上限");
				NoticeText.text = "容量已达上限";
				Notice.SetActive(true);
				//NoticeView.Instance.ShowNotice("容量已达上限");
				return;
			}

			if (PlaygroundViewModel.Money.Value <DtoCache.ConfigDto.PlaygroundConfigDto.DollConfigs[id].Price) {
				Notice.SetActive(true);
				NoticeText.text = "金钱不足";
				return;
			}
			else {
				PlaygroundViewModel.Money.Value -= DtoCache.ConfigDto.PlaygroundConfigDto.DollConfigs[id].Price;
			}
			//PlaygroundViewModel.Money.Value -= DtoCache.ConfigDto.PlaygroundConfigDto.DollConfigs[id].Price;

			_count++;
			_sceneCapacity[scene]++;
			PlaygroundViewModel.Dolls.Value[id]++;
		}

		var spawnPos = compose ? _lastPos : GetRandomSpawnPoint();
		var doll     = TrashMan.spawn(Doll, spawnPos, Quaternion.identity).GetComponent<Doll>();
		doll.name                  = _count.ToString();
		doll.Id                    = id;
		doll.SpriteRenderer.sprite = PlaygroundUtil.GetSpriteById(id);
		doll.transform.SetParent(Parents[scene], false);
		AddSmoke(spawnPos);
		//AudioController.Instance.Play(Sfx);
	}

	private Vector2 GetRandomSpawnPoint() =>
		new Vector2(Random.Range(Boundary.MinX, Boundary.MaxX), Random.Range(Boundary.MinY, Boundary.MaxY));

	void OnDrawGizmos() {
		Gizmos.color = Color.white;
		var topLeft  = new Vector3(Boundary.MinX, Boundary.MaxY, transform.position.z);
		var topRight = new Vector3(Boundary.MaxX, Boundary.MaxY, transform.position.z);
		var btmRight = new Vector3(Boundary.MaxX, Boundary.MinY, transform.position.z);
		var btmLeft  = new Vector3(Boundary.MinX, Boundary.MinY, transform.position.z);

		Gizmos.DrawLine(topLeft, topRight);
		Gizmos.DrawLine(topRight, btmRight);
		Gizmos.DrawLine(btmRight, btmLeft);
		Gizmos.DrawLine(btmLeft, topLeft);
	}

	public void SetLastPos(Vector2 lastPos) => _lastPos = lastPos;

	private void AddSmoke(Vector2 spawnPos) {
		var smoke = TrashMan.spawn(Smoke, spawnPos, Quaternion.identity);
		TrashMan.despawnAfterDelay(smoke, 2f);
	}
}