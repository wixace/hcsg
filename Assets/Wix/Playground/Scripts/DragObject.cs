using UnityEngine;

namespace Assets.Shaddock.Scripts.Effects.Object {
	public class DragObject : MonoBehaviour {

		private Vector3 _screenPoint;
		private Vector3 _offset;
		private Vector3 _startPos;
		private Vector3 _finishPos;

		public Boundary Boundary;

		void OnMouseDown() {
			_screenPoint=UnityEngine.Camera.main.WorldToScreenPoint(gameObject.transform.position);
			_offset=gameObject.transform.position-UnityEngine.Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,Input.mousePosition.y,_screenPoint.z));
			_startPos=transform.position;
			

		}

		/*void Update() 
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		
		if (Physics.Raycast(ray, out hit, 10000)) 
			Debug.DrawLine(ray.origin, hit.point);
	}*/
		void OnMouseDrag() {
			Vector3 cursorPoint = new Vector3(Input.mousePosition.x,Input.mousePosition.y,_screenPoint.z);
			//Vector3 cursorPosition = UnityEngine.Camera.main.ScreenToWorldPoint(cursorPoint)+_offset;
			Vector3 cursorPosition = UnityEngine.Camera.main.ScreenToWorldPoint(cursorPoint);
			//transform.position=new Vector3(cursorPosition.x,cursorPosition.y,cursorPosition.z);
			transform.position=new Vector3(Mathf.Clamp(cursorPosition.x,Boundary.MinX,Boundary.MaxX),Mathf.Clamp(cursorPosition.y,Boundary.MinY,Boundary.MaxY),cursorPosition.z);


		}
		protected virtual void OnMouseUp() {

			_finishPos=transform.position;
			//if(Vector3.Distance(_startPos,_finishPos)>distanceToMove)
			//	transform.position=Calc(transform.position.x,transform.position.y,transform.position.z);

			transform.position=new Vector3(transform.position.x,transform.position.y,transform.position.z);
			//	print ("释放鼠标"+transform.position);

		}
		Vector3 Calc(float x,float y,float z) {
			float tempx, tempz;
			float desx, desz;

			//x = transform.position.x;
			tempx=(x%5);
			if(tempx>2.5) {
				desx=((int)(x/5)+1)*5;
				print(desx);
			} else {
				desx=((int)x/5-1)*5;
				print(desx);
			}
			tempz=(z%5);
			if(tempz>2.5) {
				desz=((int)z/5+1)*5;
				print(desz);
			} else {
				desz=((int)z/5-1)*5;
				print(desz);
			}
			return new Vector3(desx,y,desz);
		}

		
	}

	[System.Serializable]
	public struct Boundary
	{
		public float MaxY,MinY,MaxX,MinX;
	}
}