using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Shaddock.Scripts.Foundation.Base {
	public abstract class BaseBehaviour : MonoBehaviour {
		
		#region UI Behaviour

		protected bool CheckGuiRaycastObjects(GraphicRaycaster graphicRaycaster)
		{

			if (graphicRaycaster != null)
			{
				PointerEventData eventData = new PointerEventData(EventSystem.current);
				eventData.pressPosition = Input.mousePosition;
				eventData.position = Input.mousePosition;
				List<RaycastResult> list = new List<RaycastResult>();
				graphicRaycaster.Raycast(eventData, list);

				//UIManager.BuildingUI.GetComponent<GraphicRaycaster> ().Raycast(eventData, list);
				//Main.Instance.graphicRaycaster.Raycast
				//Debug.Log(list.Count);

				return list.Count > 0;
			}
			return false;
		}
	}
		#endregion

	}

